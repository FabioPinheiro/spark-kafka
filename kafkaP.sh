#! /bin/bash
end=$((SECONDS+60))

COUNT=$((SECONDS))
while [ $SECONDS -lt $end ]; do
    if [ $SECONDS -gt $COUNT ]
    then
        echo $SECONDS $COUNT $(($COUNT*30)) Clinks
        COUNT=$((SECONDS))
        cat log_format3.txt | kafka-console-producer.sh --broker-list localhost:9092 --topic topicName
    fi
done