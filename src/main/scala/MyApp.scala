import java.text.SimpleDateFormat

import kafka.serializer.StringDecoder
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.StreamingContext

import scala.concurrent.Await
import scala.util.parsing.json.{JSON, JSONObject, JSONType}


case class HiveTableStr(a:Int,b:String,d:String,c:String)

trait Hack {
  import scala.reflect.runtime.universe._
  def classAccessors[T: TypeTag]: List[String] =
    typeOf[T].members.collect {case m: MethodSymbol if m.isCaseAccessor => m}.toList.map(_.name.toString).reverse
}

object Main extends Hack{
  def main(args: Array[String]): Unit = {
    println(classAccessors[HiveTableStr])
  }
}

object MyApp {
  val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  val fieldToGet = "@timestamp" :: "page_id" :: "is_bot" :: "country_id" :: Nil
  val logFile = "log_format.txt"
  def main(args: Array[String]) {
    val conf = new SparkConf().setMaster("local[2]").setAppName("The Greatest Spark Application")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Spark.SparkStreamingBatchDuration)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    val spark = SparkSession.builder().config(conf).getOrCreate()
    //val sqlHiveContext = new org.apache.spark.sql.SQLContext(sc) = new org.apache.spark.sql.hive.HiveContext(sc)


    val rddApp1 = ssc.sparkContext.emptyRDD[(Int,Int)]
    val count = 0
    var dfApp1 = sqlContext.createDataFrame(rddApp1).toDF("country_id","count")
      //.createOrReplaceTempView("tmp_spark")

    val stream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, Kafka.ConsumerParams, Set(Kafka.Topic))
      .map(m => m._2)

    stream.foreachRDD{
      (rdd,ts) =>
        println("foreachRDD #################################################################")
        val data = rdd.map{ s =>
          val json = JSON.parseRaw(s)
          json match {
            case None => throw new RuntimeException("HUM?!")
            case Some(data) => data match {
              case JSONObject(o) =>
                val timestamp = format.parse(o.get("@timestamp").get.toString).getTime
                val page_id = o.get("page_id").get.toString
                val is_bot = o.get("is_bot").get.toString.toBoolean
                val country_id = o.get("country_id").get.toString.toInt
                var rest = o.dropWhile(s => fieldToGet.contains(s._1)).toArray
                (timestamp,page_id,is_bot,country_id) //FIXME rest java.lang.UnsupportedOperationException: Schema for type Any is not supported
              case _ => throw new RuntimeException("No Json Object?!")
            }
          }
        }.map{e =>
          e //TODO Write to Kafa
        }


        //### I'm I hive sink ##########################################################
        //data.collect().map{ e =>println(s"ts=$ts, ${e._1}, ${e._2}, ${e._3}, ${e._4}, ${e._5}")}
        val name = fieldToGet.map(e => if (e == "@timestamp") "timestamp" else e )
        val df = sqlContext.createDataFrame(data).toDF(name:_*)
        df.printSchema()
        df.show()

        //import org.apache.spark.sql.hive.HiveContext; //TODO
        //HiveContext sqlContext = new org.apache.spark.sql.hive.HiveContext(sc.sc()); //TODO
        //df.write().mode("append").saveAsTable("schemaName.tableName"); //TODO



        //### APP 1 ###
        val sqlfunc = org.apache.spark.sql.functions.udf(()=>1)
        val dfAux1 = df.select(df.col("country_id")).withColumn("count",sqlfunc())
        dfApp1 = dfApp1.union(dfAux1).groupBy(dfApp1.col("country_id")).agg("count" -> "sum").toDF("country_id","count")
        println("### APP1 ###")
        dfApp1.show()


        /*  //### APP with SQL ###
        df.createOrReplaceTempView("tmp_spark_app2")
        spark.sql(
          """SELECT *
            |FROM tmp_spark_app2
            |WHERE page_id == 'homepage'
            |ORDER BY country_id ASC""".stripMargin
        ).show()
        */

    }

    sys.ShutdownHookThread {
      println("Gracefully stopping Spark Streaming Application")
      //spark.sql("""SELECT * FROM tmp_spark""").show()
      ssc.stop(true, true)
      println("Application stopped")
      spark.stop()
      sc.stop()
    }

    println("START")
    ssc.start()
    println("STOP")
    ssc.awaitTerminationOrTimeout(Spark.SparkContextDuration.milliseconds) //1m
    println("END")
  }
}
