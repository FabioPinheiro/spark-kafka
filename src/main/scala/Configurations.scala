import org.apache.spark.streaming.Seconds


object Configurations extends Spark with Kafka
object Spark extends Spark
object Kafka extends Kafka



trait Spark {
  val SparkStreamingBatchDuration = Seconds(5)
  //CONFIG
  val SparkContextDuration = Seconds(120) //CONFIG
}

trait Kafka {
  val Host = "localhost"
  val Topic = "topicName"
  val ConsumerName = "kafka_consumer"

  //CONFIG
  val Partitions = 1
  val DecoderEncoding = "UTF8"

  val GrupeId = "group1"
  //CONFIG
  val Brokers = Host + ":9092"

  //CONFIG
  val Zookeeper = Host + ":2181" //CONFIG
  def ConsumerParams = Map[String, String](
    "metadata.broker.list" -> Brokers,
    "bootstrap.servers" -> Brokers,
    "group.id" -> this.GrupeId,
    "auto.offset.reset" -> "smallest"
  )
}