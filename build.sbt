name := "Spark-Kafka"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.0.2"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.0.2"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.0.2"
libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka-0-8" % "2.0.0"

//libraryDependencies += "org.apache.spark" %% "spark-hive" % "2.0.2"
//libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.10"
//libraryDependencies += "net.liftweb" %% "lift-json" % "3.0"
